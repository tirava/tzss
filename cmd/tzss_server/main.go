package main

import (
	"context"
	"flag"
	"fmt"
	"math/rand"
	"os"
	"path/filepath"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/tirava/tzss/internal/configs"
	"gitlab.com/tirava/tzss/internal/domain/services"
	"gitlab.com/tirava/tzss/internal/http"
	"gitlab.com/tirava/tzss/internal/storages"
)

func main() {
	rand.Seed(time.Now().UnixNano())

	fileName := filepath.Base(os.Args[0])
	flag.Usage = func() {
		fmt.Printf("Start http server: %s [-config=configFile]\n", fileName)
		flag.PrintDefaults()
	}

	configCmd := flag.String("config", "config.yml", "path to yaml config file")
	flag.Parse()

	config, err := configs.NewConfig(*configCmd)
	if err != nil {
		log.Fatal(err)
	}

	db, err := storages.NewDB(context.TODO(), config.DBType, config.DSN)
	if err != nil {
		log.Fatal(err)
	}

	userService := services.NewUserService(db)

	sessionTime, err := time.ParseDuration(config.SessionTime)
	if err != nil {
		log.Fatal(err)
	}

	lg := log.New()
	lg.Level = log.Level(config.LogLevel - 1) // nolint:gomnd
	log.Printf("Logger started at mode: %s", lg.Level)

	http.StartHTTPServer(config.ListenHTTP, lg, userService, sessionTime)

	if err := db.CloseDB(); err != nil {
		log.Fatal(err)
	}
}
