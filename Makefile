.PHONY: build run docker-build docker-run up down restart test

build:
	go build -o app/tzss_server cmd/tzss_server/*.go

run:
	./app/tzss_server

docker-build:
	docker build -t tzss_server -f ./build/package/tzss_server/Dockerfile .

docker-run:
	docker run --rm -d -p 8080:8080 --name tzss_server tzss_server app/tzss_server

up:
	docker-compose up -d --build

down:
	docker-compose down

restart: down up

test:
	set -e ;\
    docker-compose -f docker-compose.test.yml up --no-start --build ;\
    test_status_code=0 ;\
    docker-compose -f docker-compose.test.yml run tzss-integration-tests || test_status_code=$$? ;\
    docker-compose -f docker-compose.test.yml down ;\
    exit $$test_status_code ;\