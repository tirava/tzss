package http

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"regexp"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/tirava/tzss/internal/domain/entities"
	"gitlab.com/tirava/tzss/internal/domain/services"
)

const (
	reqIDField        = "request_id"
	hostField         = "host"
	methodField       = "method"
	urlField          = "url"
	browserField      = "browser"
	remoteField       = "remote"
	queryField        = "query"
	codeField         = "response_code"
	respTimeField     = "response_time"
	userIDField       = "user_id"
	sessionIDField    = "session_id"
	sessionCookieName = "TZSS-Session"
)

type handler struct {
	handlers      map[string]http.HandlerFunc
	cacheHandlers map[string]*regexp.Regexp
	logger        *log.Logger
	userService   *services.UserService
	sessionTime   time.Duration
	error         Error
}

// @title TZSS API
// @version 0.1
// @description This is a sample server TZSS server.
// @termsOfService http://swagger.io/terms/

// @contact.name API Support
// @contact.url http://www.swagger.io/support
// @contact.email support@swagger.io

// @license.name Apache 2.0
// @license.url http://www.apache.org/licenses/LICENSE-2.0.html

// @host localhost:8080
// @BasePath /api/v1

// help struct for swagger
// nolint:deadcode, structcheck, unused
type newUser struct {
	name     string
	email    string
	password string
}

func newHandlers(logger *log.Logger, userService *services.UserService, sessionTime time.Duration) *handler {
	return &handler{
		handlers:      make(map[string]http.HandlerFunc),
		cacheHandlers: make(map[string]*regexp.Regexp),
		logger:        logger,
		userService:   userService,
		sessionTime:   sessionTime,
		error:         newError(logger),
	}
}

func (h handler) helloHandler(w http.ResponseWriter, r *http.Request) {
	query := r.URL.Query()
	name := query.Get("name")

	if name == "" {
		name = "Stachka-2020"
	}

	h.logger.WithFields(log.Fields{
		codeField: http.StatusOK, reqIDField: getRequestID(r.Context()),
	}).Infof("RESPONSE")

	s := "Hello, my name is " + name

	if _, err := io.WriteString(w, s); err != nil {
		h.logger.Errorf("[hello] error write to response writer")
	}
}

//@Title RegisterUser
//@Description register new user
//@Param	user	body newUser	true	"{name, email, password}"
//@Accept  json
//@Produce  json
//@Success 201 {object} entities.User
//@Failure 400 {object} Error
//@Failure 500 {object} Error
//@Router /users/register [post]
func (h handler) registerUserHandler(w http.ResponseWriter, r *http.Request) {
	var errDecoder string

	reqID := getRequestID(r.Context())

	user := entities.NewUser()
	if err := json.NewDecoder(r.Body).Decode(user); err != nil {
		errDecoder = err.Error()
	}

	if errDecoder != "" || user.Name == "" || user.Email == "" || user.Password == "" {
		err := fmt.Errorf("incomplete user fields or bad data format: %s", errDecoder)
		h.logger.WithFields(log.Fields{
			codeField: http.StatusBadRequest, reqIDField: reqID, userIDField: user.ID,
		}).Errorf(err.Error())
		h.error.send(w, http.StatusBadRequest, err, "error while parse user fields")

		return
	}

	if err := h.userService.RegisterUser(r.Context(), user); err != nil {
		h.logger.WithFields(log.Fields{
			codeField: http.StatusInternalServerError, reqIDField: reqID, userIDField: user.ID,
		}).Errorf(err.Error())
		h.error.send(w, http.StatusInternalServerError, err, "error while register user into DB")

		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)

	user.Password = "*****"
	if err := json.NewEncoder(w).Encode(user); err != nil {
		h.logger.WithFields(log.Fields{
			reqIDField: reqID, userIDField: user.ID,
		}).Errorf(err.Error())
	}

	h.logger.WithFields(log.Fields{userIDField: user.ID}).Infof("user successfully registered into DB")

	h.logger.WithFields(log.Fields{
		codeField: http.StatusCreated, reqIDField: reqID}).Debugf("RESPONSE")
}

//@Title LoginUser
//@Description auth user
//@Param	user	body newUser	true	"{email, password}"
//@Accept  json
//@Produce  json
//@Success 201 {object} entities.User
//@Failure 400 {object} Error
//@Failure 401 {object} Error
//@Router /users/login [post]
func (h handler) loginUserHandler(w http.ResponseWriter, r *http.Request) {
	reqID := getRequestID(r.Context())

	user := &entities.User{}
	if err := json.NewDecoder(r.Body).Decode(user); err != nil {
		h.logger.WithFields(log.Fields{
			codeField: http.StatusBadRequest, reqIDField: reqID,
		}).Errorf(err.Error())
		h.error.send(w, http.StatusBadRequest, err, "error while parse user fields")

		return
	}

	user.SessionEnd = time.Now().Add(h.sessionTime)

	updatedUser, err := h.userService.LoginUser(r.Context(), user)
	if err != nil {
		h.logger.WithFields(log.Fields{
			codeField: http.StatusUnauthorized, reqIDField: reqID,
		}).Errorf(err.Error())
		h.error.send(w, http.StatusUnauthorized, err, "user not authenticated")

		return
	}

	http.SetCookie(w, &http.Cookie{
		Name:    sessionCookieName,
		Value:   updatedUser.SessionID.String(),
		Path:    "/",
		Expires: updatedUser.SessionEnd,
	})

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusCreated)

	updatedUser.Password = "*****"
	if err := json.NewEncoder(w).Encode(updatedUser); err != nil {
		h.logger.WithFields(log.Fields{
			reqIDField: reqID, userIDField: updatedUser.ID,
		}).Errorf(err.Error())
	}

	h.logger.WithFields(log.Fields{
		userIDField: updatedUser.ID, sessionIDField: updatedUser.SessionID.String(),
	}).Infof("user session successfully updated in DB")

	h.logger.WithFields(log.Fields{codeField: http.StatusCreated, reqIDField: reqID}).Debugf("RESPONSE")
}

//@Title LogoutUser
//@Description logout user
//@Success 200
//@Failure 400 {object} Error
//@Failure 401 {object} Error
//@Router /users/logout [get]
func (h handler) logoutUserHandler(w http.ResponseWriter, r *http.Request) {
	reqID := getRequestID(r.Context())

	sessionID, err := r.Cookie(sessionCookieName)
	if err != nil {
		h.logger.WithFields(log.Fields{
			codeField: http.StatusBadRequest, reqIDField: reqID,
		}).Errorf(err.Error())
		h.error.send(w, http.StatusBadRequest,
			errors.New("unable to logout"), "session cookie not presented")

		return
	}

	if err := h.userService.LogoutUser(r.Context(), sessionID.Value); err != nil {
		h.logger.WithFields(log.Fields{
			codeField: http.StatusUnauthorized, reqIDField: reqID,
		}).Errorf(err.Error())
		h.error.send(w, http.StatusUnauthorized, err, "user not authenticated")

		return
	}

	http.SetCookie(w, &http.Cookie{Name: sessionCookieName, MaxAge: -1})

	h.logger.WithFields(log.Fields{
		codeField: http.StatusOK, reqIDField: reqID,
	}).Debugf("RESPONSE")
}

//@Title Get random number
//@Description show random int
//@Success 200
//@Failure 401 {object} Error
//@Router /users/rnd [get]
func (h handler) getRndNumberHandler(w http.ResponseWriter, r *http.Request) {
	if !h.isUserAuthed(w, r) {
		return
	}

	if _, err := io.WriteString(w, h.userService.GetRndNumber()); err != nil {
		h.logger.WithFields(log.Fields{
			codeField: http.StatusInternalServerError, reqIDField: getRequestID(r.Context()),
		}).Errorf(err.Error())

		return
	}

	h.logger.WithFields(log.Fields{
		codeField:  http.StatusOK,
		reqIDField: getRequestID(r.Context()),
	}).Debugf("RESPONSE")
}

func (h handler) isUserAuthed(w http.ResponseWriter, r *http.Request) bool {
	reqID := getRequestID(r.Context())

	sessionID, err := r.Cookie(sessionCookieName)
	if err != nil {
		h.logger.WithFields(log.Fields{
			codeField: http.StatusUnauthorized, reqIDField: reqID,
		}).Errorf(err.Error())
		h.error.send(w, http.StatusUnauthorized,
			errors.New("user not authenticated"), "session cookie not presented, login please")

		return false
	}

	ok, err := h.userService.IsUserSessionValid(r.Context(), sessionID.Value)
	if err != nil {
		h.logger.WithFields(log.Fields{
			codeField: http.StatusUnauthorized, reqIDField: getRequestID(r.Context()),
		}).Errorf(err.Error())
		h.error.send(w, http.StatusUnauthorized, err, "user session is not valid")

		return false
	}

	if !ok {
		h.logger.WithFields(log.Fields{
			codeField: http.StatusUnauthorized, reqIDField: getRequestID(r.Context()),
		}).Errorf("session expired in DB")
		h.error.send(w, http.StatusUnauthorized, err, "user session is expired, re-login please")
	}

	return ok
}
