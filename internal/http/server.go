// Package http implements http server.
package http

import (
	"context"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"

	log "github.com/sirupsen/logrus"
	"gitlab.com/tirava/tzss/internal/domain/services"
)

const (
	readTimeout    = 10 * time.Second
	writeTimeout   = 10 * time.Second
	maxHeaderBytes = 1 << 20
)

// StartHTTPServer inits routing and starts web listener.
func StartHTTPServer(listen string, logger *log.Logger, userService *services.UserService,
	sessionTime time.Duration) {
	handlers := newHandlers(logger, userService, sessionTime)
	srv := &http.Server{
		Addr:           listen,
		Handler:        handlers.prepareRoutes(listen),
		ReadTimeout:    readTimeout,
		WriteTimeout:   writeTimeout,
		MaxHeaderBytes: maxHeaderBytes,
	}

	shutdown := make(chan os.Signal)
	signal.Notify(shutdown, os.Interrupt, syscall.SIGTERM)

	ctx, cancel := context.WithCancel(context.Background())

	go func() {
		defer cancel()
		logger.Warnf("Signal received: %s", <-shutdown)

		if err := srv.Shutdown(ctx); err != nil {
			logger.Errorf("Error while shutdown server: %s", err)
		}
	}()

	logger.Infof("Starting HTTP server at: %s", listen)

	if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		logger.Errorf(err.Error())
		// nolint:gomnd
		os.Exit(1)
	}

	logger.Infof("Shutdown HTTP server at: %s", listen)
}
