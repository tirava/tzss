package http

import (
	"context"
	"fmt"
	"net/http"
	"regexp"
	"strings"
	"time"

	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
	httpSwagger "github.com/swaggo/http-swagger"

	_ "gitlab.com/tirava/tzss/docs" // add swagger docs
)

const contextKeyRequestID contextKey = "requestID"

type contextKey string

func (h handler) prepareRoutes(listen string) http.Handler {
	s := strings.Split(listen, ":")
	if s[0] == "" {
		s[0] = "localhost"
	}

	siteMux := http.NewServeMux()

	h.addPath("GET /hello($|/.*)", h.helloHandler)
	h.addPath("GET /swagger(/.*)",
		httpSwagger.Handler(httpSwagger.URL(fmt.Sprintf("http://%s:%s/swagger/doc.json", s[0], s[1]))))

	h.addPath("POST /api/v1/users/register$", h.registerUserHandler)
	h.addPath("POST /api/v1/users/login$", h.loginUserHandler)
	h.addPath("GET /api/v1/users/logout$", h.logoutUserHandler)
	h.addPath("GET /api/v1/users/rnd$", h.getRndNumberHandler)

	siteHandler := h.pathMiddleware(siteMux)
	siteHandler = h.loggerMiddleware(siteHandler)
	siteHandler = h.panicMiddleware(siteHandler)

	return siteHandler
}

func (h handler) addPath(regex string, handler http.HandlerFunc) {
	h.handlers[regex] = handler
	cache, err := regexp.Compile(regex)

	if err != nil {
		log.Fatal(err)
	}

	h.cacheHandlers[regex] = cache
}

func (h handler) pathMiddleware(http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		check := r.Method + " " + r.URL.Path
		for pattern, handlerFunc := range h.handlers {
			if h.cacheHandlers[pattern].MatchString(check) {
				handlerFunc(w, r)
				return
			}
		}
		h.logger.WithFields(log.Fields{
			codeField: http.StatusNotFound, reqIDField: getRequestID(r.Context()), urlField: r.URL.Path,
		}).Errorf("RESPONSE")
		http.NotFound(w, r)
	})
}

func (h handler) panicMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		h.logger.Tracef("Middleware 'panic' PASS")
		defer func() {
			if err := recover(); err != nil {
				h.logger.Panicf("recovered from panic: %s", err)
				http.Error(w, http.StatusText(http.StatusInternalServerError), http.StatusInternalServerError)
			}
		}()
		next.ServeHTTP(w, r)
	})
}

func (h handler) loggerMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx := assignRequestID(r.Context())
		r = r.WithContext(ctx)
		h.logger.WithFields(requestFields(
			r, reqIDField, hostField, methodField, urlField, browserField, remoteField, queryField,
		)).Debugf("REQUEST START")
		start := time.Now()
		next.ServeHTTP(w, r)
		h.logger.WithFields(log.Fields{
			respTimeField: time.Since(start), reqIDField: getRequestID(ctx), urlField: r.URL.Path,
		}).Debugf("REQUEST END")
	})
}

func getRequestID(ctx context.Context) string {
	reqID := ctx.Value(contextKeyRequestID)
	if key, ok := reqID.(string); ok {
		return key
	}

	return ""
}

func requestFields(r *http.Request, args ...string) log.Fields {
	fields := make(log.Fields)

	for _, s := range args {
		switch s {
		case reqIDField:
			fields[reqIDField] = getRequestID(r.Context())
		case hostField:
			fields[hostField] = r.Host
		case methodField:
			fields[methodField] = r.Method
		case urlField:
			fields[urlField] = r.URL.Path
		case browserField:
			fields[browserField] = r.Header.Get("User-Agent")
		case remoteField:
			fields[remoteField] = r.RemoteAddr
		case queryField:
			fields[queryField] = r.URL.RawQuery
		}
	}

	return fields
}

func assignRequestID(ctx context.Context) context.Context {
	reqID := uuid.New()
	return context.WithValue(ctx, contextKeyRequestID, reqID.String())
}
