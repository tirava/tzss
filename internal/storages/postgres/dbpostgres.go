// Package postgres implements postgres DB interface.
package postgres

import (
	"context"
	"errors"
	"fmt"
	"time"

	"github.com/google/uuid"
	_ "github.com/jackc/pgx/stdlib" // driver for postgres
	"github.com/jmoiron/sqlx"
	"gitlab.com/tirava/tzss/internal/domain/entities"
)

// DBPostgres is the base struct for using postgres DB.
type DBPostgres struct {
	db         *sqlx.DB
	usersTable string
}

// NewPostgresDB returns new postgres db struct.
func NewPostgresDB(ctx context.Context, dsn, usersTable string) (*DBPostgres, error) {
	db, err := sqlx.Open("pgx", dsn)
	if err != nil {
		return nil, fmt.Errorf("error open db: %w", err)
	}

	err = db.PingContext(ctx)
	if err != nil {
		return nil, fmt.Errorf("error ping db: %w", err)
	}

	dbPg := &DBPostgres{
		db:         db,
		usersTable: usersTable,
	}

	return dbPg, nil
}

// AddUser adds user to postgres db.
func (db *DBPostgres) AddUser(ctx context.Context, user *entities.User) error {
	user.SessionEnd = time.Now().UTC()
	query := fmt.Sprintf(`insert into %s
		(id, name, email, password, sessionend) values(:id, :name, :email, :password, :sessionend)`,
		db.usersTable)

	result, err := db.db.NamedExecContext(ctx, query, user)
	if err != nil {
		return fmt.Errorf("error named exec while adding user: %w", err)
	}

	ra, err := result.RowsAffected()
	if err != nil {
		return fmt.Errorf("error rows affected while adding user: %w", err)
	}

	if ra != 1 { // nolint:gomnd
		return errors.New("user not inserted into db: no rows affected")
	}

	return nil
}

func (db *DBPostgres) getUserBy(ctx context.Context, query string, user *entities.User) (*entities.User, error) {
	rows, err := db.db.NamedQueryContext(ctx, query, user)
	if err != nil {
		return user, fmt.Errorf("error named query gitting user: %w", err)
	}

	if !rows.Next() {
		return &entities.User{}, nil
	}

	if err := rows.StructScan(&user); err != nil {
		return user, fmt.Errorf("error scan struct while getting user: %w", err)
	}

	if err := rows.Close(); err != nil {
		return user, fmt.Errorf("error close rows while getting user: %w", err)
	}

	return user, nil
}

// GetUserByEmail returns user by email.
func (db *DBPostgres) GetUserByEmail(ctx context.Context, email string) (*entities.User, error) {
	user := &entities.User{Email: email}
	query := fmt.Sprintf("select * from %s where email=:email", db.usersTable)

	return db.getUserBy(ctx, query, user)
}

// GetUserBySessionID returns user by sessionID.
func (db *DBPostgres) GetUserBySessionID(ctx context.Context, sessionID uuid.UUID) (*entities.User, error) {
	user := &entities.User{SessionID: sessionID}
	query := fmt.Sprintf("select * from %s where sessionid=:sessionid", db.usersTable)

	return db.getUserBy(ctx, query, user)
}

// UpdateUserSession updates user session id.
func (db *DBPostgres) UpdateUserSession(ctx context.Context, user *entities.User) error {
	user.SessionEnd = user.SessionEnd.UTC()
	query := fmt.Sprintf(`update %s set sessionid=:sessionid, sessionend=:sessionend where id=:id`, db.usersTable)

	result, err := db.db.NamedExecContext(ctx, query, user)
	if err != nil {
		return fmt.Errorf("error named exec while update user session: %w", err)
	}

	ra, err := result.RowsAffected()
	if err != nil {
		return fmt.Errorf("error rows affected while updating user session: %w", err)
	}

	if ra != 1 { // nolint:gomnd
		return errors.New("error while updating user session")
	}

	return nil
}

// CloseDB closes postgres db.
func (db *DBPostgres) CloseDB() error {
	if err := db.db.Close(); err != nil {
		return fmt.Errorf("error close DB: %w", err)
	}

	return nil
}
