// Package storages implements storage interface.
package storages

import (
	"context"
	"fmt"

	"gitlab.com/tirava/tzss/internal/domain/interfaces/storage"
	"gitlab.com/tirava/tzss/internal/storages/inmemory"
	"gitlab.com/tirava/tzss/internal/storages/postgres"
)

// NewDB returns DB by db type.
func NewDB(ctx context.Context, dbType, dsn string) (storage.DB, error) {
	switch dbType {
	case "postgres":
		return postgres.NewPostgresDB(ctx, dsn, "users")
	case "inmemory":
		return inmemory.NewMapDB()
	default:
		return nil, fmt.Errorf("incorrect storage db type name: %s", dbType)
	}
}
