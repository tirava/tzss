/*
 * HomeWork-8: Calendar protobuf preparation
 * Created on 24.10.2019 19:12
 * Copyright (c) 2019 - Eugene Klimov
 */

// Package inmemory implements memory DB interface.
package inmemory

import (
	"context"
	"errors"
	"sync"
	"time"

	"github.com/google/uuid"
	"gitlab.com/tirava/tzss/internal/domain/entities"
)

// DBMap is the base struct for using map db.
type DBMap struct {
	sync.RWMutex
	users map[uuid.UUID]entities.User
}

// NewMapDB returns new map db storage.
func NewMapDB() (*DBMap, error) {
	dbm := &DBMap{
		users: make(map[uuid.UUID]entities.User),
	}

	return dbm, nil
}

// AddUser adds user to map db.
func (db *DBMap) AddUser(ctx context.Context, user *entities.User) error {
	db.Lock()
	defer db.Unlock()

	if _, ok := db.users[user.ID]; ok {
		return errors.New("user already exists")
	}

	user.SessionEnd = time.Now().UTC()
	db.users[user.ID] = *user

	return nil
}

// UpdateUserSession updates user session id.
func (db *DBMap) UpdateUserSession(ctx context.Context, user *entities.User) error {
	db.Lock()
	defer db.Unlock()

	if _, ok := db.users[user.ID]; !ok {
		return errors.New("user not found")
	}

	user.SessionEnd = user.SessionEnd.UTC()
	newUser := db.users[user.ID]
	newUser.SessionID = user.SessionID
	newUser.SessionEnd = user.SessionEnd
	db.users[user.ID] = newUser

	return nil
}

// GetUserByEmail returns user by email.
func (db *DBMap) GetUserByEmail(ctx context.Context, email string) (*entities.User, error) {
	db.Lock()
	defer db.Unlock()

	for k, v := range db.users {
		if v.Email == email {
			user := db.users[k]
			return &user, nil
		}
	}

	return &entities.User{}, nil
}

// GetUserBySessionID returns user by sessionID.
func (db *DBMap) GetUserBySessionID(ctx context.Context, sessionID uuid.UUID) (*entities.User, error) {
	db.Lock()
	defer db.Unlock()

	for k, v := range db.users {
		if v.SessionID == sessionID {
			user := db.users[k]
			return &user, nil
		}
	}

	return &entities.User{}, nil
}

// CloseDB closes storage.
func (db *DBMap) CloseDB() error {
	return nil
}
