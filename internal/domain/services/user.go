// Package services implements user's service operations.
package services

import (
	"context"
	"crypto/sha256"
	"encoding/hex"
	"errors"
	"fmt"
	"math/rand"
	"strconv"
	"time"

	"github.com/google/uuid"
	"gitlab.com/tirava/tzss/internal/domain/entities"
	"gitlab.com/tirava/tzss/internal/domain/interfaces/storage"
)

// UserService is the main user service struct.
type UserService struct {
	db storage.DB
}

// NewUserService inits main service fields.
func NewUserService(db storage.DB) *UserService {
	return &UserService{
		db: db,
	}
}

// RegisterUser registers new user into DB.
func (u *UserService) RegisterUser(ctx context.Context, user *entities.User) error {
	checkUser, err := u.db.GetUserByEmail(ctx, user.Email)
	if err != nil {
		return fmt.Errorf("error get user by email: %w", err)
	}

	if checkUser.Email != "" {
		return errors.New("user with this email already exists")
	}

	user.Password, err = u.hashString(user.Password)
	if err != nil {
		return fmt.Errorf("error hash password: %w", err)
	}

	if err := u.db.AddUser(ctx, user); err != nil {
		return fmt.Errorf("error add user into db: %w", err)
	}

	return nil
}

// LoginUser auths user and saves token into DB.
func (u *UserService) LoginUser(ctx context.Context, user *entities.User) (*entities.User, error) {
	checkUser, err := u.db.GetUserByEmail(ctx, user.Email)
	if err != nil {
		return nil, fmt.Errorf("error get user by email: %w", err)
	}

	if user.Email == "" {
		return nil, errors.New("need not empty email")
	}

	if checkUser.Email != user.Email {
		return nil, errors.New("user not found")
	}

	hashUserPassword, err := u.hashString(user.Password)
	if err != nil {
		return nil, fmt.Errorf("error hash password for auth user: %w", err)
	}

	if checkUser.Password != hashUserPassword {
		return nil, errors.New("invalid password")
	}

	checkUser.SessionID = uuid.New()
	checkUser.SessionEnd = user.SessionEnd

	if err := u.db.UpdateUserSession(ctx, checkUser); err != nil {
		return nil, fmt.Errorf("error update user session: %w", err)
	}

	return checkUser, nil
}

// IsUserSessionValid checks user session is valid.
func (u *UserService) IsUserSessionValid(ctx context.Context, sessionID string) (bool, error) {
	session, err := uuid.Parse(sessionID)
	if err != nil {
		return false, fmt.Errorf("error parse session UUID while validation: %w", err)
	}

	checkUser, err := u.db.GetUserBySessionID(ctx, session)
	if err != nil {
		return false, fmt.Errorf("error get user by sessionID: %w", err)
	}

	dateNil := time.Time{}
	if checkUser.SessionEnd == dateNil {
		return false, errors.New("user session not found")
	}

	if time.Since(checkUser.SessionEnd) > 0 {
		return false, nil
	}

	return true, nil
}

// LogoutUser logs user out.
func (u *UserService) LogoutUser(ctx context.Context, sessionID string) error {
	session, err := uuid.Parse(sessionID)
	if err != nil {
		return fmt.Errorf("error parse session UUID while logout: %w", err)
	}

	logoutUser, err := u.db.GetUserBySessionID(ctx, session)
	if err != nil {
		return fmt.Errorf("error get user by sessionID: %w", err)
	}

	dateNil := time.Time{}
	if logoutUser.SessionEnd == dateNil {
		return errors.New("user session not found")
	}

	logoutUser.SessionEnd = time.Now()
	if err := u.db.UpdateUserSession(ctx, logoutUser); err != nil {
		return fmt.Errorf("error update user session for logout: %w", err)
	}

	return nil
}

// GetRndNumber returns random number in string.
func (u *UserService) GetRndNumber() string {
	return strconv.Itoa(rand.Int()) // nolint:gosec
}

func (u *UserService) hashString(data string) (string, error) {
	hasher := sha256.New()

	if _, err := hasher.Write([]byte(data)); err != nil {
		return "", fmt.Errorf("error hash string: %w", err)
	}

	return hex.EncodeToString(hasher.Sum(nil)), nil
}
