package services

import (
	"context"
	"log"
	"testing"
	"time"

	"github.com/google/uuid"
	"gitlab.com/tirava/tzss/internal/domain/entities"
	"gitlab.com/tirava/tzss/internal/storages"
)

// nolint:gochecknoglobals
var (
	userService *UserService
	ctx         = context.TODO()
)

// nolint:gochecknoinits
func init() {
	db, err := storages.NewDB(context.TODO(), "inmemory", "")
	if err != nil {
		log.Fatal(err)
	}

	userService = NewUserService(db)
}

// nolint:funlen
func TestRegisterUser(t *testing.T) {
	var testCases = []struct {
		description string
		user        *entities.User
		expectError bool
		checkUser   bool
	}{
		{
			"Registering first user",
			&entities.User{
				ID:       uuid.New(),
				Name:     "Test Testovich",
				Email:    "test@fake.com",
				Password: "secret",
			},
			false,
			true,
		},
		{
			"Registering second user with same email",
			&entities.User{
				ID:       uuid.New(),
				Name:     "Test1 Testovich1",
				Email:    "test@fake.com",
				Password: "secret1",
			},
			true,
			false,
		},
	}

	for _, test := range testCases {
		origPassword := test.user.Password
		err := userService.RegisterUser(ctx, test.user)

		if !test.expectError && err != nil {
			t.Errorf("%s should not return error but returns:\n%s", test.description, err)
		}

		if test.expectError && err == nil {
			t.Errorf("%s should return error but not returns.", test.description)
		}

		if test.checkUser {
			registeredUser, err := userService.db.GetUserByEmail(ctx, test.user.Email)
			{
				if err != nil {
					t.Errorf("Getting user by session ID should not return error but returns:\n%s", err)
				}
			}

			if test.user.Name != registeredUser.Name || test.user.Email != registeredUser.Email {
				t.Errorf("Registered user name or email not equal original:\n"+
					"test.user.Name = %s, registeredUser.Name = %s\n"+
					"test.user.Email = %s, registeredUser.Email = %s",
					test.user.Name, registeredUser.Name, test.user.Email, registeredUser.Email)
			}

			if origPassword == registeredUser.Password {
				t.Errorf("Registered user password should be hashed but equal original:\n"+
					"origPassword = %s, registeredUser.Password = %s",
					origPassword, registeredUser.Password)
			}
		}
	}
}

func TestLoginUser(t *testing.T) {
	user := entities.NewUser()
	user.Name = "authName"
	user.Email = "auth@user.com"
	user.Password = "authPassword"
	origPassword := user.Password

	if err := userService.RegisterUser(ctx, user); err != nil {
		t.Fatalf("Registering user should not return error but returns:\n%s", err)
	}

	var testCases = []struct {
		description string
		user        *entities.User
		expectError bool
	}{
		{
			"Auth user with empty email",
			&entities.User{
				Email:    "",
				Password: user.Password,
			},
			true,
		},
		{
			"Auth user not found",
			&entities.User{
				Email:    "qqq@www.ee",
				Password: user.Password,
			},
			true,
		},
		{
			"Auth user with invalid password",
			&entities.User{
				Email:    user.Email,
				Password: "secret",
			},
			true,
		},
		{
			"Auth successfully",
			&entities.User{
				Email:    user.Email,
				Password: origPassword,
			},
			false,
		},
	}

	for _, test := range testCases {
		_, err := userService.LoginUser(ctx, test.user)

		if !test.expectError && err != nil {
			t.Errorf("%s should not return error but returns:\n%s", test.description, err)
		}

		if test.expectError && err == nil {
			t.Errorf("%s should return error but not returns.", test.description)
		}
	}
}

// nolint:funlen, gomnd
func TestIsUserSessionValid(t *testing.T) {
	user := entities.NewUser()
	user.Name = "sessionName"
	user.Email = "session@user.com"
	user.Password = "sessionPassword"
	origPassword := user.Password

	if err := userService.RegisterUser(ctx, user); err != nil {
		t.Fatalf("Registering user should not return error but returns:\n%s", err)
	}

	user.Password = origPassword
	user.SessionEnd = time.Now().Add(time.Millisecond * 500)
	authUser, err := userService.LoginUser(ctx, user)

	if err != nil {
		t.Fatalf("Auth user should not return error but returns:\n%s", err)
	}

	var testCases = []struct {
		description    string
		sessionID      string
		sleep          time.Duration
		expectError    bool
		isSessionValid bool
	}{
		{
			"Invalid session ID format",
			"111-222-333",
			0,
			true,
			false,
		},
		{
			"Session ID not found",
			uuid.New().String(),
			0,
			true,
			false,
		},
		{
			"Session valid",
			authUser.SessionID.String(),
			0,
			false,
			true,
		},
		{
			"Session expired",
			authUser.SessionID.String(),
			time.Millisecond * 1000,
			false,
			false,
		},
	}

	for _, test := range testCases {
		time.Sleep(test.sleep)
		ok, err := userService.IsUserSessionValid(ctx, test.sessionID)

		if !test.expectError && err != nil {
			t.Errorf("%s should not return error but returns:\n%s", test.description, err)
		}

		if test.expectError && err == nil {
			t.Errorf("%s should return error but not returns.", test.description)
		}

		if !test.isSessionValid && ok {
			t.Errorf("%s should not return valid session but returns:\n%v", test.description, ok)
		}

		if test.isSessionValid && !ok {
			t.Errorf("%s should return valid session but returns:\n%v", test.description, ok)
		}
	}
}

// nolint:gomnd
func TestLogoutUser(t *testing.T) {
	user := entities.NewUser()
	user.Name = "sessionEndName"
	user.Email = "sessionEnd@user.com"
	user.Password = "sessionEndPassword"
	origPassword := user.Password

	if err := userService.RegisterUser(ctx, user); err != nil {
		t.Fatalf("Registering user should not return error but returns:\n%s", err)
	}

	user.Password = origPassword
	user.SessionEnd = time.Now().Add(time.Millisecond * 500)
	authUser, err := userService.LoginUser(ctx, user)

	if err != nil {
		t.Fatalf("Auth user should not return error but returns:\n%s", err)
	}

	session := authUser.SessionID.String()
	if err = userService.LogoutUser(ctx, session); err != nil {
		t.Fatalf("Logout user should not return error but returns:\n%s", err)
	}

	ok, err := userService.IsUserSessionValid(ctx, session)
	if err != nil {
		t.Fatalf("Session validation should not return error but returns:\n%s", err)
	}

	if ok {
		t.Errorf("Session validation should not return valid session but returns:\n%v", ok)
	}
}
