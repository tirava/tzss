// Package storage describes storage interfaces.
package storage

import (
	"context"

	"github.com/google/uuid"
	"gitlab.com/tirava/tzss/internal/domain/entities"
)

// DB is thw main interface for any DB.
type DB interface {
	AddUser(context.Context, *entities.User) error
	GetUserByEmail(context.Context, string) (*entities.User, error)
	GetUserBySessionID(context.Context, uuid.UUID) (*entities.User, error)
	UpdateUserSession(context.Context, *entities.User) error
	CloseDB() error
}
