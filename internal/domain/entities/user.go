// Package entities implements entity models.
package entities

import (
	"time"

	"github.com/google/uuid"
)

// User is the base user's struct.
type User struct {
	ID         uuid.UUID
	Name       string
	Email      string
	Password   string
	SessionID  uuid.UUID
	SessionEnd time.Time
}

// NewUser returns new user struct.
func NewUser() *User {
	return &User{ID: uuid.New()}
}
