// Package configs implements yaml config.
package configs

import (
	"fmt"
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

// Config is the main config struct.
type Config struct {
	confPath    string `yaml:"-"`
	LogLevel    int    `yaml:"log_level"`
	DBType      string `yaml:"db_type"`
	ListenHTTP  string `yaml:"http_listen"`
	SessionTime string `yaml:"session_timeout"`
	DSN         string `yaml:"dsn"`
}

// NewConfig creates new config struct.
func NewConfig(confPath string) (*Config, error) {
	conf := &Config{
		confPath: confPath,
	}

	return conf, conf.readParameters()
}

// readParameters reads config from file.
func (c *Config) readParameters() error {
	yamlFile, err := ioutil.ReadFile(c.confPath)
	if err != nil {
		return fmt.Errorf("error reading config file: %w", err)
	}

	err = yaml.Unmarshal(yamlFile, c)
	if err != nil {
		return fmt.Errorf("error unmarshal config file: %w", err)
	}

	// nolint:gomnd
	if c.LogLevel < 1 || c.LogLevel > 7 {
		c.LogLevel = 5
	}

	if c.DBType == "" {
		c.DBType = "inmemory"
	}

	if c.ListenHTTP == "" {
		c.ListenHTTP = "localhost:8080"
	}

	if c.SessionTime == "" {
		c.SessionTime = "1h"
	}

	return nil
}
