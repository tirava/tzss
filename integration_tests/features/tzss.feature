# file: features/previewer.feature

# http://tzss-server:8080

Feature: TZSS server
	As http client of tzss service
	In order to understand that the user got random numbers from service
	I want to authenticate on tzss service
	And it must give me random number for every request

	Scenario: TZSS service is available
		When I send "GET" request to "http://tzss-server:8080/hello/"
		Then The response code should be 200
		And The response should match text "Hello, my name is Stachka-2020"

	Scenario: GET random number without auth
		When I send "GET" request to "http://tzss-server:8080/api/v1/users/rnd"
		Then The response code should be 401
		And The response should contains text "user not authenticated"

	Scenario: Registering user
		When I send "POST" request to "http://tzss-server:8080/api/v1/users/register" with "application/json" data:
        """
        {
        	"Name": "Test Testovich",
        	"Email": "test@fake.dom",
        	"Password": "secret"
        }
        """
		Then The response code should be 201
		And The response should contains text "Test Testovich"

	Scenario: Authenticating user
    		When I send "POST" request to "http://tzss-server:8080/api/v1/users/login" with "application/json" data:
            """
            {
            	"Email": "test@fake.dom",
            	"Password": "secret"
            }
            """
    		Then The response code should be 201
    		And The response should contains text "Test Testovich"

	Scenario: GET random number after auth
		When I send "GET" request to "http://tzss-server:8080/api/v1/users/rnd"
		Then The response code should be 200
		And The response should contains random integer number

	Scenario: Logout user
		When I send "GET" request to "http://tzss-server:8080/api/v1/users/logout"
		Then The response code should be 200

	Scenario: GET random number after logout
		When I send "GET" request to "http://tzss-server:8080/api/v1/users/rnd"
		Then The response code should be 401
		And The response should contains text "user session is expired"