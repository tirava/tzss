package main

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"

	"github.com/DATA-DOG/godog/gherkin"

	"github.com/DATA-DOG/godog"
)

type tzssTest struct {
	responseStatusCode int
	responseBody       []byte
	cookie             http.Cookie
}

func (t *tzssTest) iSendRequestTo(httpMethod, addr string) error {
	var r *http.Response

	var err error

	switch httpMethod {
	case http.MethodGet:
		var req *http.Request

		req, err = http.NewRequest(http.MethodGet, addr, nil)
		if err != nil {
			return err
		}

		req.AddCookie(&t.cookie)

		client := &http.Client{}
		// nolint:gosec, bodyclose
		r, err = client.Do(req)
	default:
		err = fmt.Errorf("unknown method: %s", httpMethod)
	}

	if err != nil {
		return err
	}
	defer r.Body.Close()

	t.responseStatusCode = r.StatusCode
	t.responseBody, err = ioutil.ReadAll(r.Body)

	if err != nil {
		return err
	}

	return nil
}

func (t *tzssTest) theResponseCodeShouldBe(code int) error {
	if t.responseStatusCode != code {
		return fmt.Errorf("unexpected status code: %d != %d", t.responseStatusCode, code)
	}

	return nil
}

func (t *tzssTest) theResponseShouldMatchText(text string) error {
	if string(t.responseBody) != text {
		return fmt.Errorf("unexpected text: %s != %s", t.responseBody, text)
	}

	return nil
}

func (t *tzssTest) theResponseShouldContainsText(text string) error {
	if !strings.Contains(string(t.responseBody), text) {
		return fmt.Errorf("unexpected text: %s !contains %s", t.responseBody, text)
	}

	return nil
}

func (t *tzssTest) iSendRequestToWithData(httpMethod, addr, contentType string, data *gherkin.DocString) error {
	var r *http.Response

	var err error

	switch httpMethod {
	case http.MethodPost:
		replacer := strings.NewReplacer("\n", "", "\t", "")
		cleanJSON := replacer.Replace(data.Content)
		// nolint:gosec, bodyclose
		r, err = http.Post(addr, contentType, bytes.NewReader([]byte(cleanJSON)))
	default:
		err = fmt.Errorf("unknown method: %s", httpMethod)
	}

	if err != nil {
		return err
	}
	defer r.Body.Close()

	t.responseBody, err = ioutil.ReadAll(r.Body)
	if err != nil {
		return err
	}

	t.responseStatusCode = r.StatusCode
	if len(r.Cookies()) > 0 {
		t.cookie = *r.Cookies()[0]
	}

	return nil
}

func (t *tzssTest) theResponseShouldContainsRandomIntegerNumber() error {
	str := string(t.responseBody)

	number, err := strconv.Atoi(str)
	if err != nil {
		return fmt.Errorf("unexpected result: %s != Int", str)
	}

	if number <= 0 {
		return fmt.Errorf("unexpected result: %d <= 0", number)
	}

	return nil
}

func FeatureContext(s *godog.Suite) {
	test := new(tzssTest)

	s.Step(`^I send "([^"]*)" request to "([^"]*)"$`, test.iSendRequestTo)
	s.Step(`^The response code should be (\d+)$`, test.theResponseCodeShouldBe)
	s.Step(`^The response should match text "([^"]*)"$`, test.theResponseShouldMatchText)
	s.Step(`^The response should contains text "([^"]*)"$`, test.theResponseShouldContainsText)
	s.Step(`^I send "([^"]*)" request to "([^"]*)" with "([^"]*)" data:$`, test.iSendRequestToWithData)
	s.Step(`^The response should contains random integer number$`,
		test.theResponseShouldContainsRandomIntegerNumber)
}
