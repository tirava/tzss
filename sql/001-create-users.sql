create table if not exists users (
	id UUID primary key,
	name text,
	email text,
	password text,
	sessionid UUID,
	sessionend timestamp not null
);
