# Тестовое задание на вакансию Go-специалист

Разработать сервис REST API. Который имеет 3 метода:

1. Регистрация пользователя
2. Авторизация
3. Получение случайного числа для авторизированных пользователей

Пользователей регистрировать в любой БД.

**Результат**: сервис должен запускаться в *Docker*.
Для поднятия всей инфраструктуры (сам сервис, БД и т.п.) должен запускаться `docker-compose`.

### Пример использования
1. Некоторые параметра конфигурирования находятся в `config.yml`
2. Запуск сервиса через `make up`, остановка и очистка - `make down`
3. Сервис по умолчанию будет доступен на [http://localhost:8080](http://localhost:8080)
4. Быстрая проверка состояния - [http://localhost:8080/hello](http://localhost:8080/hello)
5. Регистрация пользователя - [http://localhost:8080/api/v1/users/register](http://localhost:8080/api/v1/users/register)

    Метод *POST*, данные в теле запроса в *json*, например:

`{
   "Name": "user",
   "Email": "user@domain.com",
   "Password": "password"
 }`

6. Авторизация (аутентификация) пользователя - [http://localhost:8080/api/v1/users/login](http://localhost:8080/api/v1/users/login)

    Метод *POST*, данные в теле запроса в *json*, например:

`{
   "Email": "user@domain.com",
   "Password": "password"
 }`

7. Получение случайного числа для авторизированных пользователей - [http://localhost:8080/api/v1/users/rnd](http://localhost:8080/api/v1/users/rnd)

   Метод *GET*

8. Заверешение сессии пользователя - [http://localhost:8080/api/v1/users/logout](http://localhost:8080/api/v1/users/logout)

   Метод *GET*

9. Запуск интеграционных тестов через `make test`
10. Swagger - [http://localhost:8080/swagger/index.html](http://localhost:8080/swagger/index.html)

